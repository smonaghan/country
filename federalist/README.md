# Underpinnings of the American Project
- [Project Website](https://sites.google.com/view/underpinnings-american-project/home?authuser=0)
- [The Federalist Papers Course at Hillsdale College](https://online.hillsdale.edu/courses/the-federalist-papers)

## Resources
- [How-Money-Drives-US-Congressional-Elections-More-Evidence.pdf](https://www.ineteconomics.org/uploads/papers/How-Money-Drives-US-Congressional-Elections-More-Evidence.pdf)
- [The Project Gutenberg EBook of The Federalist Papers](http://gutenberg.org/cache/epub/18/pg18.txt)