# Roll Call Vote 117th Congress - 1st Session
- [Question: Guilty or Not Guilty (Article of Impeachment Against Former President Donald John Trump )](https://www.senate.gov/legislative/LIS/roll_call_lists/roll_call_vote_cfm.cfm?congress=117&session=1&vote=00059#top)
- [H.Res.24 - Impeaching Donald John Trump, President of the United States, for high crimes and misdemeanors
](https://www.congress.gov/bill/117th-congress/house-resolution/24)

| Guilty | Not Guilty |
| -- | -- |
| [Baldwin (D-WI)](https://www.opensecrets.org/members-of-congress/summary?cid=N00004367&cycle=2020) | [Barrasso (R-WY)](https://www.opensecrets.org/members-of-congress/summary?cid=N00006236&cycle=2020) |
| [Bennet (D-CO)](https://www.opensecrets.org/members-of-congress/summary?cid=N00030608&cycle=2020) | [Blackburn (R-TN)](https://www.opensecrets.org/members-of-congress/summary?cid=N00003105&cycle=2020) |
| [Blumenthal (D-CT)](https://www.opensecrets.org/politicians/summary.php?cid=N00031685&cycle=2020) | [Blunt (R-MO)](https://www.opensecrets.org/politicians/summary.php?cid=N00005195&cycle=2020) |
| [Booker (D-NJ)](https://www.opensecrets.org/politicians/summary.php?cid=N00035267&cycle=2020) | [Boozman (R-AR)](https://www.opensecrets.org/politicians/summary.php?cid=N00013873&cycle=2020) |
| [Brown (D-OH)](https://www.opensecrets.org/politicians/summary.php?cid=N00003535&cycle=2020) | [Braun (R-IN)](https://www.opensecrets.org/members-of-congress/summary?cid=N00041731&cycle=2020) |
| [**Burr (R-NC)**](https://www.opensecrets.org/members-of-congress/summary?cid=N00002221&cycle=2020) | [Capito (R-WV)](https://www.opensecrets.org/politicians/summary.php?cid=N00009771&cycle=2020) |
| [Cantwell (D-WA)](https://www.opensecrets.org/politicians/summary.php?cid=N00007836&cycle=2020) | [Cornyn (R-TX)](https://www.opensecrets.org/politicians/summary.php?cid=N00024852&cycle=2020) |
| [Cardin (D-MD)](https://www.opensecrets.org/politicians/summary.php?cid=N00001955&cycle=2020) | [Cotton (R-AR)](https://www.opensecrets.org/politicians/summary.php?cid=N00033363&cycle=2020) |
| [Carper (D-DE)](https://www.opensecrets.org/politicians/summary.php?cid=N00012508&cycle=2020) | [Cramer (R-ND)](https://www.opensecrets.org/politicians/summary.php?cid=N00004614&cycle=2020) |
| [Casey (D-PA)](https://www.opensecrets.org/politicians/summary.php?cid=N00027503&cycle=2020) | [Crapo (R-ID)](https://www.opensecrets.org/politicians/summary.php?cid=N00006267&cycle=2020) |
| [**Cassidy (R-LA)**](https://www.opensecrets.org/politicians/summary.php?cid=N00030245&cycle=2020) | [Cruz (R-TX)](https://www.opensecrets.org/politicians/summary.php?cid=N00033085&cycle=2020) |
| [**Collins (R-ME)**](https://www.opensecrets.org/politicians/summary.php?cid=N00000491&cycle=2020) | [Daines (R-MT)](https://www.opensecrets.org/politicians/summary.php?cid=N00033054&cycle=2020) |
| [Coons (D-DE)](https://www.opensecrets.org/politicians/summary.php?cid=N00031820&cycle=2020) | [Ernst (R-IA)](https://www.opensecrets.org/politicians/summary.php?cid=N00035483&cycle=2020) |
| [Cortez Masto (D-NV)](https://www.opensecrets.org/politicians/summary.php?cid=N00037161&cycle=2020) | [Fischer (R-NE)](https://www.opensecrets.org/politicians/summary.php?cid=N00033443&cycle=2020) |
| [Duckworth (D-IL)](https://www.opensecrets.org/politicians/summary.php?cid=N00027860&cycle=2020) | [Graham (R-SC)](https://www.opensecrets.org/politicians/summary.php?cid=N00009975&cycle=2020) |
| [Durbin (D-IL)](https://www.opensecrets.org/politicians/summary.php?cid=N00004981&cycle=2020) | [Grassley (R-IA)](https://www.opensecrets.org/politicians/summary.php?cid=N00001758&cycle=2020) |
| [Feinstein (D-CA)](https://www.opensecrets.org/politicians/summary.php?cid=N00007364&cycle=2020) | [Hagerty (R-TN)](https://www.opensecrets.org/races/summary?cycle=2020&id=TNS2) |
| [Gillibrand (D-NY)](https://www.opensecrets.org/politicians/summary.php?cid=N00027658&cycle=2020) | [Hawley (R-MO)](https://www.opensecrets.org/politicians/summary.php?cid=N00041620&cycle=2020) |
| [Hassan (D-NH)](https://www.opensecrets.org/politicians/summary.php?cid=N00038397&cycle=2020) | [Hoeven (R-ND)](https://www.opensecrets.org/politicians/summary.php?cid=N00031688&cycle=2020) |
| [Heinrich (D-NM)](https://www.opensecrets.org/politicians/summary.php?cid=N00029835&cycle=2020) | [Hyde-Smith (R-MS)](https://www.opensecrets.org/politicians/summary.php?cid=N00043298&cycle=2020) |
| [Hickenlooper (D-CO)](https://www.opensecrets.org/races/summary?cycle=2020&id=COS2) | [Inhofe (R-OK)](https://www.opensecrets.org/politicians/summary.php?cid=N00005582&cycle=2020) |
| [Hirono (D-HI)](https://www.opensecrets.org/politicians/summary.php?cid=N00028139&cycle=2020) | [Johnson (R-WI)](https://www.opensecrets.org/politicians/summary.php?cid=N00032546&cycle=2020) |
| [Kaine (D-VA)](https://www.opensecrets.org/politicians/summary.php?cid=N00033177&cycle=2020) | [Kennedy (R-LA)](https://www.opensecrets.org/politicians/summary.php?cid=N00026823&cycle=2020) |
| [Kelly (D-AZ)](https://www.opensecrets.org/politicians/summary.php?cid=N00044223&cycle=2020) | [Lankford (R-OK)](https://www.opensecrets.org/politicians/summary.php?cid=N00031129&cycle=2020) |
| [**King (I-ME)**](https://www.opensecrets.org/politicians/summary.php?cid=N00034580&cycle=2020) | [Lee (R-UT)](https://www.opensecrets.org/politicians/summary.php?cid=N00031696&cycle=2020) |
| [Klobuchar (D-MN)](https://www.opensecrets.org/politicians/summary.php?cid=N00027500&cycle=2020) | [Lummis (R-WY)](https://www.opensecrets.org/politicians/summary.php?cid=N00029788&cycle=2016) |
| [Leahy (D-VT)](https://www.opensecrets.org/politicians/summary.php?cid=N00009918&cycle=2020) | [Marshall (R-KS)](https://www.opensecrets.org/politicians/summary.php?cid=N00037034&cycle=2020) |
| [Lujan (D-NM)](https://www.opensecrets.org/politicians/summary.php?cid=N00029562&cycle=2020) | [McConnell (R-KY)](https://www.opensecrets.org/politicians/summary.php?cid=N00003389&cycle=2020) |
| [Manchin (D-WV)](https://www.opensecrets.org/politicians/summary.php?cid=N00032838&cycle=2020) | [Moran (R-KS)](https://www.opensecrets.org/politicians/summary.php?cid=N00005282&cycle=2020) |
| [Markey (D-MA)](https://www.opensecrets.org/politicians/summary.php?cid=N00000270&cycle=2020) | [Paul (R-KY)](https://www.opensecrets.org/politicians/summary.php?cid=N00030836&cycle=2020) |
| [Menendez (D-NJ)](https://www.opensecrets.org/politicians/summary.php?cid=N00000699&cycle=2020) | [Portman (R-OH)](https://www.opensecrets.org/politicians/summary.php?cid=N00003682&cycle=2020) |
| [Merkley (D-OR)](https://www.opensecrets.org/politicians/summary.php?cid=N00029303&cycle=2020) | [Risch (R-ID)](https://www.opensecrets.org/politicians/summary.php?cid=N00029441&cycle=2020) |
| [**Murkowski (R-AK)**](https://www.opensecrets.org/politicians/summary.php?cid=N00026050&cycle=2020) | [Rounds (R-SD)](https://www.opensecrets.org/politicians/summary.php?cid=N00035187&cycle=2020) |
| [Murphy (D-CT)](https://www.opensecrets.org/politicians/summary.php?cid=N00027566&cycle=2020) | [Rubio (R-FL)](https://www.opensecrets.org/politicians/summary.php?cid=N00030612&cycle=2020) |
| [Murray (D-WA)](https://www.opensecrets.org/politicians/summary.php?cid=N00007876&cycle=2020) | [Scott (R-FL)](https://www.opensecrets.org/politicians/summary.php?cid=N00043290&cycle=2020) |
| [Ossoff (D-GA)](https://www.opensecrets.org/races/summary?cycle=2020&id=GAS1) | [Scott (R-SC)](https://www.opensecrets.org/politicians/summary.php?cid=N00031782&cycle=2020) |
| [Padilla (D-CA)](https://www.opensecrets.org/races/summary?cycle=2020&id=CA03) | [Shelby (R-AL)](https://www.opensecrets.org/politicians/summary.php?cid=N00009920&cycle=2020) |
| [Peters (D-MI)](https://www.opensecrets.org/politicians/summary.php?cid=N00029277&cycle=2020) | [Sullivan (R-AK)](https://www.opensecrets.org/politicians/summary.php?cid=N00035774&cycle=2020) |
| [Reed (D-RI)](https://www.opensecrets.org/politicians/summary.php?cid=N00000362&cycle=2020) | [Thune (R-SD)](https://www.opensecrets.org/politicians/summary.php?cid=N00004572&cycle=2020) |
| [**Romney (R-UT)**](https://www.opensecrets.org/politicians/summary.php?cid=N00000286&cycle=2020) | [Tillis (R-NC)](https://www.opensecrets.org/politicians/summary.php?cid=N00035492&cycle=2020) |
| [Rosen (D-NV)](https://www.opensecrets.org/politicians/summary.php?cid=N00038734&cycle=2020) | [Tuberville (R-AL)](https://www.opensecrets.org/races/summary?cycle=2020&id=ALS1) |
| [**Sanders (I-VT)**](https://www.opensecrets.org/politicians/summary.php?cid=N00000528&cycle=2020) | [Wicker (R-MS)](https://www.opensecrets.org/politicians/summary.php?cid=N00003280&cycle=2020) |
| [**Sasse (R-NE)**](https://www.opensecrets.org/politicians/summary.php?cid=N00035544&cycle=2020) | [Young (R-IN)](https://www.opensecrets.org/politicians/summary.php?cid=N00030670&cycle=2020) |
| [Schatz (D-HI)](https://www.opensecrets.org/politicians/summary.php?cid=N00028138&cycle=2020) | |
| [Schumer (D-NY)](https://www.opensecrets.org/politicians/summary.php?cid=N00001093&cycle=2020) | |
| [Shaheen (D-NH)](https://www.opensecrets.org/politicians/summary.php?cid=N00024790&cycle=2020) | |
| [Sinema (D-AZ)](https://www.opensecrets.org/politicians/summary.php?cid=N00033983&cycle=2020) | |
| [Smith (D-MN)](https://www.opensecrets.org/politicians/summary.php?cid=N00042353&cycle=2020) | |
| [Stabenow (D-MI)](https://www.opensecrets.org/politicians/summary.php?cid=N00004118&cycle=2020) | |
| [Tester (D-MT)](https://www.opensecrets.org/politicians/summary.php?cid=N00027605&cycle=2020) | |
| [**Toomey (R-PA)**](https://www.opensecrets.org/politicians/summary.php?cid=N00001489&cycle=2020) | |
| [Van Hollen (D-MD)](https://www.opensecrets.org/politicians/summary.php?cid=N00013820&cycle=2020) | |
| [Warner (D-VA)](https://www.opensecrets.org/politicians/summary.php?cid=N00002061&cycle=2008) | |
| [Warnock (D-GA)](https://www.opensecrets.org/races/summary?id=GAS2&cycle=2020&spec=Y) | |
| [Warren (D-MA)](https://www.opensecrets.org/politicians/summary.php?cid=N00033492&cycle=2020) | |
| [Whitehouse (D-RI)](https://www.opensecrets.org/politicians/summary.php?cid=N00027533&cycle=2020) | |
| [Wyden (D-OR)](https://www.opensecrets.org/politicians/summary.php?cid=N00007724&cycle=2020) |  |