# Texas vs. Pennsylvania, Georgia, Michigan & Wisconsin

https://www.texasattorneygeneral.gov/sites/default/files/images/admin/2020/Press/SCOTUSFiling.pdf

https://www.supremecourt.gov/orders/courtorders/121120zr_p860.pdf

https://cdn.donaldjtrump.com/public-files/press_assets/2020-11-09-complaint-as-filed.pdf

This document is an exploration of the content found within the commentary publication [A 2020 Election Redo in 4 States? Here Are the Details About Texas Lawsuit](https://www.heritage.org/election-integrity/commentary/2020-election-redo-4-states-here-are-the-details-about-texas-lawsuit), published by [The Heritage Foundation](https://www.heritage.org/election-integrity/commentary/2020-election-redo-4-states-here-are-the-details-about-texas-lawsuit) on Dec 8th, 2020.

## The First Complaint
According to Texas, these "amendments to States’ duly enacted election laws" violated the [Electors Clause](https://www.heritage.org/constitution/#!/articles/2/executive) of the Constitution, Art. II, § 1, Cl. 2, which vests "state legislatures with plenary authority regarding the appointment of presidential electors."

### Pennsylvania

The complaint accuses Pennsylvania Secretary of State Kathy Boockvar of, among other things, "without legislative approval, unilaterally abrogating" Pennsylvania statutes that require "signature verification for absentee or mail-in ballots." These changes were "not ratified" by the Pennsylvania legislature.

### Complaint:
> Pennsylvania’s Secretary of State, Kathy Boockvar, without legislative approval, unilaterally abrogated several Pennsylvania statutes requiring signature verification for absentee or mail-in ballots.

### Relevant Election Code:
>  "[t]he Election Code does not permit county election officials to reject applications or voted ballots based solely on  signature analysis. . . . No challenges may be made to mail-in and absentee ballots at any time based on signature analysis."

### Questions
**Q:** Did Kathy Boockvar actually **unilaterally change** PA election law?

**A:**: @me: It does not appear that she actually changed anything. [Act 77 of 2019](https://www.legis.state.pa.us/cfdocs/legis/li/uconsCheck.cfm?yr=2019&sessInd=0&act=77) of the PA election code **omits** any requirement for signature verification. What actually happened was that ["some county election boards wanted to verify mail-in ballots’ signatures"](https://thefederalist.com/2020/10/27/the-pennsylvania-supreme-courts-bad-voting-decision-is-the-laws-fault/), **directly contrary to Act 77 of 2019.**

@me Or to say it another way, during the course of the election season (based on the filing date of the petition I'm assuming that was ~10/20), **some county election boards decided that they did not aggree with the ESTABLISHED voting laws of PA, and instead, wanted signatures for mail-in ballots to be verified**. In response, the secretary of the commonwealth, Democrat Kathy Boockvar, [petitioned the court to rule]() on whether that was allowed.

> *"On October 14, 2020, our Court granted the application of the Secretary of the Commonwealth, Kathy Boockvar ("Secretary"), to assume King's Bench jurisdiction and consider her request for [declaratory relief](https://dictionary.law.com/Default.aspx?selected=448), limited to answering the following question: "Whether the Election Code authorizes or requires county election boards to reject voted absentee or mail-in ballots during pre-canvassing and canvassing based on signature  analysis where there are alleged or perceived signature variances?*

 The [PA court cited](http://www.pacourts.us/assets/opinions/Supreme/out/J-113-2020mo%20-%20104584871117842321.pdf?cb=1) the fact that Act 77 of 2019 explicitly omits any requirement for signature verification for mail-in ballots, also noting that other forms of voting, according to PA election code, do require signature verification.

**Q:** On what **date** did Kathy Boockvar unilaterally change the aforementioned component of PA election law?

**A1:** @me As noted above, it appears that she didn't actually *change* the law. What she did do, was to submit gudiance on **September 11, 2020**. See **A2.**

**A2:** According to the **September 11, 2020**  version 1.0 document [GUIDANCE CONCERNING EXAMINATION OF ABSENTEE AND
MAIL-IN BALLOT RETURN ENVELOPES](https://www.dos.pa.gov/VotingElections/OtherServicesEvents/Documents/Examination%20of%20Absentee%20and%20Mail-In%20Ballot%20Return%20Envelopes.pdf)

- *"If the Voter’s Declaration on the return envelope is signed and the county board is satisfied that the declaration is sufficient, the mail-in or absentee ballot should be approved for canvassing unless challenged in accordance with the Pennsylvania Election Code"*

- *Pennsylvania Secretary of the Commonwealth Kathy Boockvar had issued guidance **earlier this year** saying local election officials cannot toss ballots because of signature comparisons alone.* [[ref]](https://www.politico.com/news/2020/10/23/pennsylvania-court-ballot-signatures-431794)

**Q:** What was the official ruling of the court?

**A:** *"The court concluded that there was no clause in the state’s election code that allowed ballots to be rejected based on signature comparisons, and if the state’s lawmakers wanted one, they would have included it.*" [[ref]](https://www.politico.com/news/2020/10/23/pennsylvania-court-ballot-signatures-431794)

- *"It is not our role under our tripartite system of governance to engage in judicial legislation and to rewrite a statute in order to supply terms which are not present therein, and we will not do so in this instance"* [[ref]](https://casetext.com/case/in-re-nov-3-2020-general-election-1)
- [[J-113-2020] IN THE SUPREME COURT OF PENNSYLVANIA MIDDLE DISTRICT](http://www.pacourts.us/assets/opinions/Supreme/out/J-113-2020mo%20-%20104584871117842321.pdf?cb=1)
- *"...some county election boards wanted to verify mail-in ballots’ signatures, and the secretary of the commonwealth, Democrat Kathy Boockvar, petitioned the court to rule on whether that was allowed. They ruled that it was not. Their reasoning was hard to refute: [Act 77 of 2019](https://www.legis.state.pa.us/cfdocs/legis/li/uconsCheck.cfm?yr=2019&sessInd=0&act=77), which created the option for mail-in voting, does not contain any such provision. Other forms of voting do require signature verification."* [[ref]](https://thefederalist.com/2020/10/27/the-pennsylvania-supreme-courts-bad-voting-decision-is-the-laws-fault/)

### Resources
See [Page 10 of Pennsylvania Absentee and Mail-in Ballot Return Guidance](https://www.luzernecounty.org/AgendaCenter/ViewFile/Agenda/_11122020-1998)

- [Case Text - In re Nov. 3, 2020 General Election](https://casetext.com/case/in-re-nov-3-2020-general-election-1)
- [Section 3146.8 - Canvassing of Official Absentee Ballots and Mail-In Ballots](https://casetext.com/statute/pennsylvania-statutes/statutes-unconsolidated/title-25-ps-elections-electoral-districts/chapter-14-election-code/article-xiii-voting-by-qualified-absentee-electors/section-31468-canvassing-of-official-absentee-ballots-and-mail-in-ballots)
- [PENNSYLVANIA ELECTION CODE](https://www.legis.state.pa.us/WU01/LI/LI/US/PDF/1937/0/0320..PDF)
- [GUIDANCE CONCERNING EXAMINATION OF ABSENTEE AND
MAIL-IN BALLOT RETURN ENVELOPES](https://www.dos.pa.gov/VotingElections/OtherServicesEvents/Documents/Examination%20of%20Absentee%20and%20Mail-In%20Ballot%20Return%20Envelopes.pdf)
- [GUIDANCE CONCERNING CIVILIAN ABSENTEE AND MAIL‐IN BALLOT PROCEDURES](https://www.dos.pa.gov/VotingElections/OtherServicesEvents/Documents/DOS%20Guidance%20Civilian%20Absentee%20and%20Mail-In%20Ballot%20Procedures.pdf)

### Kathy Boockvar Resignation - Effective Feb. 5 2021
> Kathy Boockvar will step down as Pennsylvania's secretary of state Friday because her department did not publicly advertise a proposed constitutional amendment that would allow child sexual abuse survivors who have aged out of the statute of limitation to sue their abusers over a two-year window.

- [Gov. Wolf Announces Changes at the Department of State After Failure to Advertise a Constitutional Amendment](https://www.governor.pa.gov/newsroom/gov-wolf-announces-changes-at-the-department-of-state-after-failure-to-advertise-a-constitutional-amendment/)
- [Pa. department botches amendment aiding child sex abuse victims](https://www.phillyvoice.com/kathy-boockvar-pennsylvania-secretary-state-resignation)
- [Former Pennsylvania Secretary of State Kathy Boockvar On The Lessons She's Learned](https://www.npr.org/2021/02/11/967079538/former-pennsylvania-secretary-of-state-kathy-boockvar-on-the-lessons-shes-learne)

---

**Georgia:** Similarly, the complaint describes how Georgia’s Secretary of State, Brad Raffensperger, also "without legislative approval, unilaterally abrogated Georgia’s statute governing the signature verification process for absentee ballots."

Complaint:
> Georgia’s Secretary of State, Brad Raffensperger, without legislative approval, unilaterally abrogated Georgia’s statute governing the signature verification process for absentee ballots

---

**Michigan:** The complaint states that Michigan Secretary of State Jocelyn Benson "abrogated Michigan election statutes related to absentee ballot applications and signature verification."

Complaint:
> Michigan’s Secretary of State, Jocelyn Benson, without legislative approval, unilaterally abrogated Michigan election statutes related to absentee ballot applications and signature verification.

---

**Wisconsin:** Lastly, the Wisconsin’s elections commission made similar changes in state laws without the permission of the legislature that "weakened, or did away with, established security procedures put in place by the Wisconsin legislature to ensure absentee ballot integrity."

According to Texas, these "amendments to States’ duly enacted election laws" violated the Electors Clause of the Constitution, Art. II, § 1, Cl. 2, which vests "state legislatures with [plenary](https://www.google.com/search?q=dictionary#dobs=plenary) authority regarding the appointment of presidential electors."

---

## The Second Complaint
Second, the complaint describes how voters in different parts of these states were treated differently...
This differential treatment, says Texas, violates the [Equal Protection Clause of the Fourteenth Amendment](https://constitution.congress.gov/browse/amendment-14/section-1/). It cites the Supreme Court’s 2000 decisions in Bush v. Gore, which "prohibits the use of differential standards in the treatment and tabulation of ballots within a state."